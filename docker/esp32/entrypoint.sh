#!/bin/bash

export IDF_PATH=/app/esp-idf
export ESPIDF=$IDF_PATH
export BOARD=GENERIC
export PATH="$EXTENSA_ESP_ELF/bin:$PATH"

printf "#####################################################\n"
printf "#####################################################\n"
printf "# Starting builf of MicroPython for ESP32           #\n"
printf "#####################################################\n"
printf "#####################################################\n"

printf " ---\n ---\n ==> [MicroPython] Cloning MicroPython Git repo\n"
[ ! -d /app/micropython] && git clone --recursive https://github.com/micropython/micropython.git /app/micropython && \
cd /app/micropython
printf " ---\n ---\n ==> [MicroPython] Clonning Repo submodules (again)...\n"
git submodule update --recursive --remote

printf " ---\n ---\n ==> [MicroPython-lib] Cloning MicroPython-lib Git repo\n"
[ ! -d /app/micropython-lib ] && git clone --recursive https://github.com/micropython/micropython-lib.git /app/micropython-lib
cd /app/micropython-lib
printf " ---\n ---\n ==> [MicroPython-lib] Clonning Repo submodules (again)...\n"
git submodule update --recursive --remote

printf " ---\n ---\n ==> [esp-idf] Cloning esp-idf Git repo\n"
git clone https://github.com/espressif/esp-idf.git  $ESPIDF
printf " ---\n ---\n ==> [esp-idf] Checking out version...\n"
printf " ---\n ---\n    --> checkout: $( grep "ESPIDF_SUPHASH :=" /app/micropython/ports/esp32/Makefile | cut -d " " -f 3 )\n\n"
cd $ESPIDF
git \
    -C esp-idf \
    checkout $(grep "ESPIDF_SUPHASH :=" /app/micropython/ports/esp32/Makefile | cut -d " " -f 3)

printf " ---\n ---\n ==> [esp-idf] Fetching esp-idf submodules...\n"
git submodule update --init --recursive

export IDF_PATH=$ESPIDF
python -m pip install --user -r $IDF_PATH/requirements.txt


cd /app

## Copy modules from micropython-lib
#printf " ---\n ---\n ==> Copying libraries to ports\n"
#for port in /app/micropython/ports/*
#do
#    printf " ---\n ---\n  --> Copying to port '${port}'\n"
#    [ ! -d $port/modules ] && mkdir -p $port/modules
#    ln -s /app/micropython-lib/urllib.parse  $port/modules/urllib.parse
#done

printf " ---\n ---\n ==> Updating MicroPython submodules...\n"
cd /app/micropython
git submodule update --recursive --remote
cp -R $ESPIDF /app/micropython

printf " ---\n ---\n ==> Building mpy-cross\n"
make ${MAKEOPTS} -C /app/micropython/mpy-cross

printf " ---\n ---\n ==> Building micropython ESP32\n"
cd /app/micropython
make ${MAKEOPTS} -C ports/esp32 ESPIDF=./esp-idf

chmod 777 -R /app
