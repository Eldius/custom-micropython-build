#!/bin/bash

export IDF_PATH=/app/micropython/esp-idf

printf "#####################################################\n"
printf "#####################################################\n"
printf "# Starting builf of MicroPython for ESP32           #\n"
printf "#####################################################\n"
printf "#####################################################\n"

printf " ---\n ---\n ==> Cloning MicroPython Git repo\n"
#git clone --recursive https://github.com/Eldius/micropython.git /app/micropython && \
git clone --recursive https://github.com/micropython/micropython.git /app/micropython && \
printf " ---\n ---\n ==> Cloning MicroPython-lib Git repo\n"
git clone --recursive https://github.com/micropython/micropython-lib.git /app/micropython-lib

cd /app

# Copy modules from micropython-lib
printf " ---\n ---\n ==> Copying libraries to ports\n"
for port in /app/micropython/ports/*
do
    printf " ---\n ---\n  --> Copying to port '${port}'\n"
    [ ! -d $port/modules ] && mkdir -p $port/modules
    ln -s /app/micropython-lib/urllib.parse  $port/modules/urllib.parse
done

cd /app/micropython

printf " ---\n ---\n ==> Getting xtensa esp32...\n"
wget \
    https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
zcat \
    xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz | tar x
export PATH=$(pwd)/xtensa-esp32-elf/bin:$PATH

printf " ---\n ---\n ==> Clonning Espressif ESP IDF...\n"
git \
    clone \
    https://github.com/espressif/esp-idf.git \
    /app/esp-idf

printf " ---\n ---\n ==> Checking out version...\n"
printf " ---\n ---\n    --> checkout: $( grep "ESPIDF_SUPHASH :=" /app/micropython/ports/esp32/Makefile | cut -d " " -f 3 )\n\n"
cd /app/esp-idf
git \
    -C esp-idf \
    checkout $(grep "ESPIDF_SUPHASH :=" /app/micropython/ports/esp32/Makefile | cut -d " " -f 3)

printf " ---\n ---\n ==> Clonning Repo submodules...\n"
git submodule update --recursive --remote
cp -R /app/esp-idf /app/micropython

cd /app/micropython

printf " ---\n ---\n ==> Clonning Repo submodules (again)...\n"
git submodule update --recursive --remote

printf " ---\n ---\n ==> Building mpy-cross\n"
make ${MAKEOPTS} -C /app/micropython/mpy-cross

printf " ---\n ---\n ==> Building micropython ESP32\n"
make ${MAKEOPTS} -C ports/esp32 ESPIDF=/app/micropython/esp-idf

chmod 777 -R /app
