#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURR_DIR="${PWD}"

cd $SCRIPT_DIR

rm -rf $SCRIPT_DIR/.work
rm -rf $SCRIPT_DIR/.tmp

set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=eldius
# image name
IMAGE=micropython-builder-esp32

docker build \
    --no-cache \
    -t $USERNAME/$IMAGE:latest .

cd $CURR_DIR
