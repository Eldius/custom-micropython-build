#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURR_DIR="${PWD}"

bash $SCRIPT_DIR/build_image.sh

docker stop micropython-builder-esp32
docker rm micropython-builder-esp32

docker run -it --name micropython-builder-esp32 --entrypoint "bash" eldius/micropython-builder-esp32:latest
