FROM ubuntu

ENV MAKEOPTS="-j4"
ADD ./build/ /app
ADD ./build/tmp-file /var/cache/apt
WORKDIR /app/micropython

RUN apt-get update && \
    apt-get install -y \
        python3-pip \
        build-essential \
        libffi-dev \
        pkg-config \
        curl \
        wget \
        git && \
    pip3 install pyparsing && \
    wget https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz && \
    zcat xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz | tar x && \
    export PATH=$(pwd)/xtensa-esp32-elf/bin:$PATH && \
    git clone https://github.com/espressif/esp-idf.git && \
    git -C esp-idf checkout $(grep "ESPIDF_SUPHASH :=" /app/ports/esp32/Makefile | cut -d " " -f 3) && \
    git -C esp-idf submodule update --init components/json/cJSON components/esp32/lib components/esptool_py/esptool components/expat/expat components/lwip/lwip components/mbedtls/mbedtls components/micro-ecc/micro-ecc components/nghttp/nghttp2 && \
    make ${MAKEOPTS} -C mpy-cross

CMD [ "make" "${MAKEOPTS}" "-C" "ports/esp32 ESPIDF=$(pwd)/esp-idf" ]
ENTRYPOINT [ "make" "${MAKEOPTS}" "-C" "mpy-cross"]
