#!/bin/bash

docker rm $( docker ps -a | awk 'FNR > 1 {print $1}' )
