#!/bin/bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
#BUILD_DIR=${PROJECT_DIR}/build/repositories
BUILD_DIR=${PROJECT_DIR}/docker/esp32/.work
TMP_DIR=${PROJECT_DIR}/docker/esp32/.tmp

echo "PROJECT DIR: ${PROJECT_DIR}"
echo "BUILD DIR: ${BUILD_DIR}"

function prepare_files {
    printf " ==> Removing old build files...\n"
    [ -d $BUILD_DIR ] && rm -rf $BUILD_DIR
    [ -d $TMP_DIR ] && rm -rf $TMP_DIR

    printf " ==> Recreating build folders...\n"
    mkdir -p $BUILD_DIR
    mkdir -p $TMP_DIR

    printf " ==> Copying repositories to the build folder...\n"
}

function build_esp32_with_docker_container {
    docker-compose \
        -f docker/esp32/docker-compose.yml \
        down
    docker-compose \
        -f docker/esp32/docker-compose.yml \
        up
}

function build_docker_image {
    bash $PROJECT_DIR/docker/esp32/build_image.sh
}

prepare_files
build_docker_image

build_esp32_with_docker_container
